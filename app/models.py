from .extensions import db

class Events(db.Model):
    id = db.Column(db.Integer, primary_key = True) 
    email_subject = db.Column(db.String(100))
    email_body = db.Column(db.Text)
    timestamp = db.Column(db.Integer)
    member = db.relationship("Event_Member", back_populates="events_member")
    
    
class Events_Member(db.Model):
    id = db.Column(db.Integer, primary_key = True)     
    member_email = db.Column(db.String(200))
    event_id = db.Column(db.ForeignKey("events.id"))
    events = db.relationship("Events", back_populates = "events")
    
class Emails(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(200), unique = True)