from flask_restx import Resource, Namespace

ns = Namespace("API")

@ns.route("/hello")
class Hello(Resource):
    def get(self):
        return {"message":"Hello WOrld"}